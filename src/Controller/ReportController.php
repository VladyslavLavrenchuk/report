<?php

namespace App\Controller;

use App\Entity\Report;
use App\Entity\User;
use App\Form\ReportType;
use App\Repository\ReportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/report")
 */
class ReportController extends AbstractController
{

    /**
     * @Route("/", name="report_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        if (isset($_GET['fromDate']) && isset($_GET['toDate'])){
            $fromDate = $_GET['fromDate'];
            $toDate = $_GET['toDate'];
            var_dump($fromDate);
            var_dump($toDate);
            $fromDate = strtotime($fromDate);
            $toDate = strtotime($toDate);
            $em = $this->getDoctrine()->getManager();
            $reportRepository = $em->getRepository('App:Report')->createQueryBuilder('r')
                ->where('r.reportDate >= :date')
                ->andWhere("r.reportDate < :date2")
                ->setParameter('date', (date("Y-m-d",$fromDate)))
                ->setParameter('date2', (date("Y-m-d",$toDate)))
                ->getQuery()
                ->getResult();
            $users = $em->getRepository('App:User')->createQueryBuilder('u')
                ->getQuery()
                ->getResult();;

            return $this->render('report/index_by_user.html.twig', [
                'reports' => $reportRepository,
                'users' => $users,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
            ]);
        }else{
            $reportRepository = $em->getRepository('App:Report')->createQueryBuilder('r')
                ->getQuery()
                ->getResult();
            $users = $this->getDoctrine()
                ->getRepository(User::class)
                ->findAll();
            return $this->render('report/index_by_user.html.twig', [
                'reports' => $reportRepository,
                'users' => $users,

            ]);
        }

    }



    /**
     * @Route("/all", name="report_all_reports", methods={"GET"})
     */
    public function indexAll(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $reportRepository = $em->getRepository('App:Report')->createQueryBuilder('r')
            ->getQuery()
            ->getResult();

        return $this->render('report/index.html.twig', [
            'reports' => $reportRepository,
        ]);
    }



    /**
     * @Route("/new", name="report_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $report = new Report();
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $report->setReportdate(\DateTime::createFromFormat('m-d-Y', date("m-d-Y")));
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $report->setUserId($user);
            $entityManager->persist($report);
            $entityManager->flush();
            $this->addFlash(
                'notice',
                'Report created'
            );

            return $this->redirectToRoute('report_index');
        }

        return $this->render('report/new.html.twig', [
            'report' => $report,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="report_show", methods={"GET"})
     */
    public function show(Report $report): Response
    {
        return $this->render('report/show.html.twig', [
            'report' => $report,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="report_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Report $report): Response
    {
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);
        $reportdate = $report->getReportdate();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash(
                'notice',
                'Report created'
            );
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('report_all_reports', [
                'id' => $report->getId(),
            ]);
        }

        return $this->render('report/edit.html.twig', [
            'report' => $report,
            'form' => $form->createView(),
            'reportdate' => $reportdate,
        ]);
    }

    /**
     * @Route("/{id}", name="report_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Report $report): Response
    {
        if ($this->isCsrfTokenValid('delete'.$report->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($report);
            $entityManager->flush();
        }

        return $this->redirectToRoute('report_index');
    }
}
