<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\EditUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController

{
    protected $roleArray = [
        'USER' => 'ROLE_ADMIN',
        'ADMIN' => 'ROLE_SUPER_ADMIN',

    ];
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(): Response
    {

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $error = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $username = $entityManager->getRepository("App:User")->findBy(['username' => $form->get('username')->getData()]);
            if($username){
                $error = true;
//                $form->get('username')->addError(new FormError("Username already in use"));
            }

            $email = $entityManager->getRepository("App:User")->findBy(['email' => $form->get('email')->getData()]);
            if($email){
                $error = true;
//                $form->get('email')->addError(new FormError("Email already in use"));
            }

            if(!$error) {
                $type = $form->get('type')->getData();

                if (isset($this->roleArray[$type])) {
                    $user->addRole($this->roleArray[$type]);
                }


                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('user_index');
            }
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $type = $form->get('type')->getData();

            if(isset($this->roleArray[$type])){
                if ($type !== 'ADMIN') {
                    $user->removeRole($this->roleArray['ADMIN']);
                    $user->addRole($this->roleArray[$type]);
                }else {
                    $user->removeRole($this->roleArray['USER']);
                    $user->addRole($this->roleArray[$type]);
                }
//                $user->removeRole($this->roleArray[$type]);
//                $user->addRole($this->roleArray[$type]);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
