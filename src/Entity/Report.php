<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReportRepository")
 */
class Report
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ebs_splitting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ebs_posting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ebs_arbs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prestige_splitting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prestige_posting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prestige_arbs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $outcome_assessments;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cure_md;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hb_arbs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hb_litigations;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hb_verification_requests;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hb_denials;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hb_initiation_letters;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jason_gallinna_splitting;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $metro_arbs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $metro_mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $other_time;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $reportDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEbsSplitting(): ?int
    {
        return $this->ebs_splitting;
    }

    public function setEbsSplitting(?int $ebs_splitting): self
    {
        $this->ebs_splitting = $ebs_splitting;

        return $this;
    }

    public function getEbsPosting(): ?int
    {
        return $this->ebs_posting;
    }

    public function setEbsPosting(?int $ebs_posting): self
    {
        $this->ebs_posting = $ebs_posting;

        return $this;
    }

    public function getEbsArbs(): ?int
    {
        return $this->ebs_arbs;
    }

    public function setEbsArbs(?int $ebs_arbs): self
    {
        $this->ebs_arbs = $ebs_arbs;

        return $this;
    }

    public function getPrestigeSplitting(): ?int
    {
        return $this->prestige_splitting;
    }

    public function setPrestigeSplitting(?int $prestige_splitting): self
    {
        $this->prestige_splitting = $prestige_splitting;

        return $this;
    }

    public function getPrestigePosting(): ?int
    {
        return $this->prestige_posting;
    }

    public function setPrestigePosting(?int $prestige_posting): self
    {
        $this->prestige_posting = $prestige_posting;

        return $this;
    }

    public function getPrestigeArbs(): ?int
    {
        return $this->prestige_arbs;
    }

    public function setPrestigeArbs(?int $prestige_arbs): self
    {
        $this->prestige_arbs = $prestige_arbs;

        return $this;
    }

    public function getOutcomeAssessments(): ?int
    {
        return $this->outcome_assessments;
    }

    public function setOutcomeAssessments(?int $outcome_assessments): self
    {
        $this->outcome_assessments = $outcome_assessments;

        return $this;
    }

    public function getCureMd(): ?int
    {
        return $this->cure_md;
    }

    public function setCureMd(?int $cure_md): self
    {
        $this->cure_md = $cure_md;

        return $this;
    }

    public function getHbArbs(): ?int
    {
        return $this->hb_arbs;
    }

    public function setHbArbs(?int $hb_arbs): self
    {
        $this->hb_arbs = $hb_arbs;

        return $this;
    }

    public function getHbLitigations(): ?int
    {
        return $this->hb_litigations;
    }

    public function setHbLitigations(?int $hb_litigations): self
    {
        $this->hb_litigations = $hb_litigations;

        return $this;
    }

    public function getHbVerificationRequests(): ?int
    {
        return $this->hb_verification_requests;
    }

    public function setHbVerificationRequests(?int $hb_verification_requests): self
    {
        $this->hb_verification_requests = $hb_verification_requests;

        return $this;
    }

    public function getHbDenials(): ?int
    {
        return $this->hb_denials;
    }

    public function setHbDenials(?int $hb_denials): self
    {
        $this->hb_denials = $hb_denials;

        return $this;
    }

    public function getHbInitiationLetters(): ?int
    {
        return $this->hb_initiation_letters;
    }

    public function setHbInitiationLetters(?int $hb_initiation_letters): self
    {
        $this->hb_initiation_letters = $hb_initiation_letters;

        return $this;
    }

    public function getJasonGallinnaSplitting(): ?int
    {
        return $this->jason_gallinna_splitting;
    }

    public function setJasonGallinnaSplitting(?int $jason_gallinna_splitting): self
    {
        $this->jason_gallinna_splitting = $jason_gallinna_splitting;

        return $this;
    }

    public function getMetroArbs(): ?int
    {
        return $this->metro_arbs;
    }

    public function setMetroArbs(?int $metro_arbs): self
    {
        $this->metro_arbs = $metro_arbs;

        return $this;
    }

    public function getMetroMail(): ?int
    {
        return $this->metro_mail;
    }

    public function setMetroMail(?int $metro_mail): self
    {
        $this->metro_mail = $metro_mail;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getOtherTime(): ?int
    {
        return $this->other_time;
    }

    public function setOtherTime(?int $other_time): self
    {
        $this->other_time = $other_time;

        return $this;
    }

    public function getReportDate(): ?\DateTimeInterface
    {
        return $this->reportDate;
    }

    public function setReportDate(?\DateTimeInterface $reportDate): self
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

}
