<?php

namespace App\EventListener;

use http\Env\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class RouteCheckListener {

    private $router;

    /**
     * RouteCheckSessionListener constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {
            $request = $event->getRequest();
            if (!$request->isXmlHttpRequest()) {
                $currentRoute = $event->getRequest()->attributes->get('_route');
//                var_dump($request->getSession()->has('providerId'));
                $not_check_route = [
                    'fos_user_security_login',
//                    'report_index',
                ];
                if (!is_null($currentRoute) && !in_array($currentRoute, $not_check_route)) {
                    $response = new RedirectResponse($this->router->generate('fos_user_security_login'));
                    $event->setResponse($response);
                }
            }
        }
        else {
            return;
        }
    }
}