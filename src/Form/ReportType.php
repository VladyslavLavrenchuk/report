<?php

namespace App\Form;

use App\Entity\Report;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ebs_splitting')
            ->add('ebs_posting')
            ->add('ebs_arbs')
            ->add('prestige_splitting')
            ->add('prestige_posting')
            ->add('prestige_arbs')
            ->add('outcome_assessments')
            ->add('cure_md')
            ->add('hb_arbs')
            ->add('hb_litigations')
            ->add('hb_verification_requests')
            ->add('hb_denials')
            ->add('hb_initiation_letters')
            ->add('jason_gallinna_splitting')
            ->add('metro_arbs')
            ->add('metro_mail')
            ->add('other')
            ->add('other_time')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
        ]);
    }
}
